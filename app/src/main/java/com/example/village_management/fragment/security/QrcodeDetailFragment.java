package com.example.village_management.fragment.security;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.village_management.R;
import com.example.village_management.manager.Contextor;
import com.example.village_management.model.ImageModel;
import com.example.village_management.util.ImageProcessUtils;

import static com.example.village_management.config.IntentConfig.CAMERA_CODE;
import static com.example.village_management.config.IntentConfig.CAMERA_REQUEST_CODE;
import static com.example.village_management.config.IntentConfig.EXTERNAL_STORAGE_CODE;
import static com.example.village_management.config.IntentConfig.IMAGE_PATH;

public class QrcodeDetailFragment extends Fragment implements View.OnClickListener {

    Button btnAddPhoto;
    Button btnConfirm;
    ImageView photo;
    ImageModel imageModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_qrcode_detail, container, false);
        btnAddPhoto = rootView.findViewById(R.id.btn_add_photo);
        btnConfirm = rootView.findViewById(R.id.btn_confirm);
        photo = rootView.findViewById(R.id.img_picture);

        btnAddPhoto.setOnClickListener(this);
        btnConfirm.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_add_photo :
                openCamera(CAMERA_REQUEST_CODE);
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == CAMERA_CODE){
            if(grantResults.length < 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                // openCamera();
            } else {
                Toast.makeText(getContext(),"จำเป็นต้องได้รับอนุญาตจากกล้องเพื่อใช้กล้อง", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == CAMERA_REQUEST_CODE) {
                Bitmap image = (Bitmap) data.getExtras().get("data");
                photo.setImageBitmap(image);
                String path = ImageProcessUtils.saveImage(image, getActivity());
                imageModel = new ImageModel(1, path, IMAGE_PATH);
            }
        }
    }

    private void openCamera(int requestCode) {
        Log.d("RequestCode", String.valueOf(requestCode));
        if(ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, CAMERA_CODE);
        } if (ContextCompat.checkSelfPermission(getActivity() ,Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            Log.v("External Storage","Permission is granted");
            ActivityCompat.requestPermissions(getActivity(),new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_CODE);
        } else {
            Intent camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(camera, requestCode);
        }
    }

}