package com.example.village_management.fragment.villager;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.village_management.R;
import com.example.village_management.util.IOnBackPressed;

public class VisitorManagementFragment extends Fragment {

    private static final int DATE_TIME_DIALOG = 1;
    Button btnGenerateQr;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_visitor_management, container, false);

        btnGenerateQr = rootView.findViewById(R.id.btn_generate_qr);
        btnGenerateQr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.nav_host_fragment_container, new GenerateQrCodeFragment())
                        .commit();
            }
        });
        return rootView;
    }

}