package com.example.village_management.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.village_management.R;
import com.example.village_management.fragment.villager.VisitorManagementFragment;

import java.util.Objects;

public class VillagerFragment extends Fragment {

    ImageView imgVisitor;
    ImageView imgPayment;
    ImageView imgTapWater;
    ImageView imgElectric;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_villager, container, false);
        imgVisitor = root.findViewById(R.id.img_visitor);
        imgVisitor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.nav_host_fragment_container, new VisitorManagementFragment())
                        .commit();
            }
        });

        return root;
    }
}