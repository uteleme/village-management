package com.example.village_management.fragment.security;

import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;

import com.example.village_management.R;
import com.example.village_management.util.CameraUtils;

import java.io.IOException;

public class CameraFragment extends Fragment implements TextureView.SurfaceTextureListener {

    private static final String TAG = "CameraFragment";
    private TextureView textureViewCamera;
    private int cameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
    Camera.Parameters parameters;
    private Camera camera;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_camera, container, false);

        textureViewCamera = rootView.findViewById(R.id.textureViewCamera);
        textureViewCamera.setSurfaceTextureListener(this);

        return rootView;
    }

    @Override
    public void onSurfaceTextureAvailable(@NonNull SurfaceTexture surface, int width, int height) {
        setupCamera(width, height);
        startCameraPreview(surface);
    }

    @Override
    public void onSurfaceTextureSizeChanged(@NonNull SurfaceTexture surface, int width, int height) {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(@NonNull SurfaceTexture surface) {
        stopCamera();
        return true;
    }

    @Override
    public void onSurfaceTextureUpdated(@NonNull SurfaceTexture surface) {

    }

    private void setupCamera(int width, int height) {
        camera = CameraUtils.openCamera(cameraId);
        parameters = camera.getParameters();
        Camera.Size bestPreviewSize = CameraUtils.getBestPreviewSize(parameters.getSupportedPreviewSizes(), width, height);
        parameters.setPreviewSize(bestPreviewSize.width, bestPreviewSize.height);
        // Camera.Size bestPictureSize = CameraUtil.getBestPictureSize(parameters.getSupportedPictureSizes());
        // parameters.setPictureSize(bestPictureSize.width, bestPictureSize.height);
        parameters.setPictureSize(2560, 1920);
        if (CameraUtils.isContinuousFocusModeSupported(parameters.getSupportedFocusModes())) {
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
        }
        camera.setParameters(parameters);
        camera.setDisplayOrientation(CameraUtils.getCameraDisplayOrientation(getActivity(), cameraId));
    }

    private void startCameraPreview(SurfaceTexture surfaceTexture) {
        try {
            camera.setPreviewTexture(surfaceTexture);
            camera.startPreview();

        } catch (IOException e) {
            Log.e(TAG, "Error start camera preview: " + e.getMessage());
        }
    }

    private void stopCamera() {
        try {
            camera.stopPreview();
            camera.release();
        } catch (Exception e) {
            Log.e(TAG, "Error stop camera preview: " + e.getMessage());
        }
    }
}