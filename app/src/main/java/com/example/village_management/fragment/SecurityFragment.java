package com.example.village_management.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.village_management.R;
import com.example.village_management.activity.QrcodeActivity;
import com.example.village_management.fragment.security.QrcodeDetailFragment;
import com.example.village_management.fragment.security.SecurityReportFragment;

public class SecurityFragment extends Fragment implements View.OnClickListener {

    ImageView imgSecurity;
    ImageView imgQrScan;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_security, container, false);

        imgSecurity = root.findViewById(R.id.img_security);
        imgQrScan = root.findViewById(R.id.img_qr_code);
        imgSecurity.setOnClickListener(this);
        imgQrScan.setOnClickListener(this);

        return root;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img_security :
                getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.nav_host_fragment_container, new SecurityReportFragment())
                    .commit();
                break;
            case R.id.img_qr_code :
                Intent intent = new Intent(getContext(), QrcodeActivity.class);
                startActivity(intent);
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.nav_host_fragment_container, new QrcodeDetailFragment())
                        .commit();
                break;
        }
    }
}