package com.example.village_management.fragment.security;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.village_management.R;
import com.example.village_management.adapter.PhotoListAdapter;
import com.example.village_management.adapter.SecurityReportListAdapter;

public class SecurityReportFragment extends Fragment {

    ListView listView;
    SecurityReportListAdapter listAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_security_report, container, false);
        listView = rootView.findViewById(R.id.list_security_report);
        listAdapter = new SecurityReportListAdapter();
        listView.setAdapter(listAdapter);
        return rootView;
    }
}