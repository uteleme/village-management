package com.example.village_management.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.village_management.R;
import com.example.village_management.adapter.PhotoListAdapter;
import com.example.village_management.dao.PhotoItemCollectionDao;
import com.example.village_management.manager.Contextor;
import com.example.village_management.manager.HttpManager;
import com.example.village_management.manager.PhotoListManager;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment {

    ListView listView;
    PhotoListAdapter listAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_home, container, false);
        listView = root.findViewById(R.id.list_item);
        listAdapter = new PhotoListAdapter();
        listView.setAdapter(listAdapter);

        Call<PhotoItemCollectionDao> call = HttpManager.getInstance().getService().LoadPhotoList();
        call.enqueue(new Callback<PhotoItemCollectionDao>() {
            @Override
            public void onResponse(@NotNull Call<PhotoItemCollectionDao> call, @NotNull Response<PhotoItemCollectionDao> response) {
                if(response.isSuccessful()){
                    PhotoListManager.getInstance().setDao(response.body());
                    listAdapter.notifyDataSetChanged();
                    //Toast.makeText(Contextor.getInstance().getContext(),response.errorBody().toString(), Toast.LENGTH_LONG).show();
                } else {
                        Toast.makeText(Contextor.getInstance().getContext(),response.errorBody().toString(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<PhotoItemCollectionDao> call, Throwable t) {
                Toast.makeText(Contextor.getInstance().getContext(),t.toString(), Toast.LENGTH_LONG).show();
            }
        });

        return root;
    }
}