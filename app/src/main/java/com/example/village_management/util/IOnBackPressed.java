package com.example.village_management.util;

public interface IOnBackPressed {
    boolean onBackPressed();
}
