package com.example.village_management.manager.http;

import com.example.village_management.dao.PhotoItemCollectionDao;

import retrofit2.Call;
import retrofit2.http.POST;

public interface ApiService {
    @POST("list")
    Call<PhotoItemCollectionDao> LoadPhotoList();
}
