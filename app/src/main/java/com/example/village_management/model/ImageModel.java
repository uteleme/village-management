package com.example.village_management.model;

public class ImageModel {
    public int id;
    public String path;
    public String type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ImageModel(int id, String path, String type) {


        this.id = id;
        this.path = path;
        this.type = type;
    }
}
