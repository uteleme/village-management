package com.example.village_management.config;

public class IntentConfig {
    public static final int CAMERA_CODE = 101;
    public static final int EXTERNAL_STORAGE_CODE = 102;

    public static final int CAMERA_REQUEST_CODE = 201;

    public static final String IMAGE_PATH = "image_path";
}
