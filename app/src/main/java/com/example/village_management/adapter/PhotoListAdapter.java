package com.example.village_management.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.village_management.dao.PhotoItemDao;
import com.example.village_management.manager.PhotoListManager;
import com.example.village_management.view.PhotoListItem;

public class PhotoListAdapter extends BaseAdapter {
    @Override
    public int getCount() {
        if(PhotoListManager.getInstance().getDao() == null){
            return 0;
        }
        if(PhotoListManager.getInstance().getDao().getData() == null){
            return 0;
        }
        return PhotoListManager.getInstance().getDao().getData().size();
    }

    @Override
    public Object getItem(int position) {
        return PhotoListManager.getInstance().getDao().getData().get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return super.getViewTypeCount();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PhotoListItem item;
        if(convertView != null) item = (PhotoListItem) convertView;
        else item = new PhotoListItem(parent.getContext());

        PhotoItemDao dao = (PhotoItemDao) getItem(position);
        item.setTitle(dao.getCaption());
        item.setBody(dao.getUsername() + "\n" + dao.getCamera());
        item.setImgLiving(dao.getImageUrl());
        return item;
    }
}
