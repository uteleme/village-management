package com.example.village_management.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.village_management.view.SecurityReportListItem;

public class SecurityReportListAdapter extends BaseAdapter {
    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SecurityReportListItem item;
        if(convertView != null) item = (SecurityReportListItem) convertView;
        else item = new SecurityReportListItem(parent.getContext());

        item.addCount(String.valueOf(position+1));
        item.openCamera();
        return item;
    }
}
